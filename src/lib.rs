#[allow(warnings)]
pub fn get_disk_list() -> Vec<Vec<String>> {
    use get_sys_info::{saturating_sub_bytes, Platform, System};
    let sys = System::new();
    let mut re = vec![];
    match sys.mounts() {
        Ok(mounts) => {
            for mount in mounts.iter() {
                re.push(vec![
                    format!("{}", mount.fs_mounted_from),
                    format!("{}", mount.fs_type),
                    format!("{}", mount.fs_mounted_on),
                    format!("{}", mount.avail),
                    format!("{}", mount.total),
                ]);
            }
        }
        Err(x) => println!("\nget_disk_list error: {}", x),
    }
    re
}
