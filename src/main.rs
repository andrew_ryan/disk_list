mod lib;
fn main() {
    let list = lib::get_disk_list();
    assert_eq!(
        vec![
            vec!["C", "NTFS", "C:\\", "18.2 GB", "107.4 GB"],
            vec!["FILE", "FAT32", "D:\\", "191.1 GB", "214.7 GB"],
            vec!["DATA", "NTFS", "E:\\", "678.5 GB", "785.5 GB"]
        ],
        list
    );
}
