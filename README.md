# Get disk list information and collect into Vec
[![crates.io](https://img.shields.io/crates/v/systemstat.svg)](https://crates.io/crates/disk_list)
[![API Docs](https://docs.rs/systemstat/badge.svg)](https://docs.rs/disk_list/)
[![unlicense](https://img.shields.io/badge/un-license-green.svg?style=flat)](https://unlicense.org)

|  Supported platforms  | 
| ----------- | 
|FreeBSD|
|Linux|
|OpenBSD|
|Windows|
|macOS|

## Examples
```rust
use disk_list;
fn get(){
     let list = disk_list::get_disk_list();
    //get disk info vec![vec![mount_from,file_type,mount_on,available_space,total_space]]
     #[cfg(target_os = "windows")]
    assert_eq!(
        vec![
            vec!["C", "NTFS", "C:\\", "18.2 GB", "107.4 GB"],
            vec!["FILE", "FAT32", "D:\\", "191.1 GB", "214.7 GB"],
            vec!["DATA", "NTFS", "E:\\", "678.5 GB", "785.5 GB"]
        ],
        list
    );
    //linux
    #[cfg(target_os = "linux")]
    assert_eq!(
        vec![
            vec!["proc", "proc", "/proc", "0 B", "0 B"],
            vec!["sys", "sysfs", "/sys", "0 B", "0 B"],
            vec!["dev", "devtmpfs", "/dev", "4.1 GB", "4.1 GB"],
            vec!["run", "tmpfs", "/run", "4.1 GB", "4.1 GB"],
            vec![
                "efivarfs",
                "efivarfs",
                "/sys/firmware/efi/efivars",
                "0 B",
                "0 B"
            ],
            vec!["/dev/nvme0n1p5", "ext4", "/", "28.7 GB", "52.5 GB"],
            vec![
                "securityfs",
                "securityfs",
                "/sys/kernel/security",
                "0 B",
                "0 B"
            ],
            vec!["tmpfs", "tmpfs", "/dev/shm", "4.0 GB", "4.1 GB"],
            vec!["devpts", "devpts", "/dev/pts", "0 B", "0 B"],
            vec!["cgroup2", "cgroup2", "/sys/fs/cgroup", "0 B", "0 B"],
            vec!["pstore", "pstore", "/sys/fs/pstore", "0 B", "0 B"],
            vec!["bpf", "bpf", "/sys/fs/bpf", "0 B", "0 B"],
            vec![
                "systemd-1",
                "autofs",
                "/proc/sys/fs/binfmt_misc",
                "0 B",
                "0 B"
            ],
            vec!["hugetlbfs", "hugetlbfs", "/dev/hugepages", "0 B", "0 B"],
            vec!["mqueue", "mqueue", "/dev/mqueue", "0 B", "0 B"],
            vec!["debugfs", "debugfs", "/sys/kernel/debug", "0 B", "0 B"],
            vec!["tracefs", "tracefs", "/sys/kernel/tracing", "0 B", "0 B"],
            vec!["configfs", "configfs", "/sys/kernel/config", "0 B", "0 B"],
            vec![
                "ramfs",
                "ramfs",
                "/run/credentials/systemd-sysusers.service",
                "0 B",
                "0 B"
            ],
            vec![
                "fusectl",
                "fusectl",
                "/sys/fs/fuse/connections",
                "0 B",
                "0 B"
            ],
            vec!["tmpfs", "tmpfs", "/tmp", "4.1 GB", "4.1 GB"],
            vec!["/dev/nvme0n1p6", "ext4", "/home", "32.8 GB", "69.5 GB"],
            vec![
                "/dev/nvme0n1p3",
                "vfat",
                "/boot/efi",
                "287.7 MB",
                "313.9 MB"
            ],
            vec!["tmpfs", "tmpfs", "/run/user/1000", "821.5 MB", "821.5 MB"],
            vec![
                "gvfsd-fuse",
                "fuse.gvfsd-fuse",
                "/run/user/1000/gvfs",
                "0 B",
                "0 B"
            ],
            vec![
                "/dev/sda2",
                "fuseblk",
                "/run/media/andry/DATA",
                "670.1 GB",
                "785.5 GB"
            ],
            vec![
                "/dev/sda1",
                "vfat",
                "/run/media/andry/FILE",
                "191.1 GB",
                "214.7 GB"
            ],
            vec![
                "/dev/nvme0n1p2",
                "fuseblk",
                "/run/media/andry/C",
                "15.3 GB",
                "107.4 GB"
            ]
        ],
        list
    );
}
```
